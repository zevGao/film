import { Notification } from 'element-ui'

class Util {
  // 获取UUID
  generateUUID() {
    var d = Date.now()
    if (window.performance && typeof window.performance.now === 'function') {
      d += performance.now()
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d / 16)
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
    })
  }

  // 国内手机号码验证
  isMobile(string) {
    const pattern = /^1[3456789]\d{9}$/
    //  let pattern = /(^[\-0-9][0-9]*(.[0-9]+)?)$/;
    return Boolean(pattern.test(string))
  }

  isMobileAndEmail(str) {
    const pattern = /(^[\w.]+@(?:[a-z0-9]+(?:-[a-z0-9]+)*\.)+[a-z]{2,3}$)|(^1[3|4|5|6|7|8|9]\d{9}$)/
    return Boolean(pattern.test(str))
  }

  // 除去字符串前后空格
  trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, '')
  }

  // 验证用户名
  isUserName(str) {
    const re = /^[(a-zA-Z0-9\u4e00-\u9fa5){1}_-]{1,21}$/
    console.log(re.test(this.trim(str)))
    return re.test(this.trim(str))
  }
  isUserNameLength(str) {
    const re = /^[(a-zA-Z0-9\u4e00-\u9fa5){1}_-]{2,20}$/
    return re.test(this.trim(str))
  }
  // 验证邮箱
  isEmail(str) {
    // eslint-disable-next-line no-useless-escape
    const re = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/
    return re.test(str) === true
  }

  // 验证团体（企业/政府机构）名称
  isGroupName(str) {
    const re = /^[\u4e00-\u9fff_a-zA-Z0-9\\-]{1,50}$/
    return re.test(this.trim(str))
  }

  //  验证下单第一步项目名称
  isProjectName(str) {
    const re = /^[\u4e00-\u9fff_a-zA-Z0-9\\-]{1,20}$/
    return re.test(this.trim(str))
  }

  // 验证下单第一步标签名称
  isTagName(str) {
    const re = /^[\u4e00-\u9fff_a-zA-Z0-9\\-]{1,6}$/
    return re.test(this.trim(str))
  }

  // 判断字符长度
  getStrLength(message, MaxLenght) {
    let strlenght = 0 // 初始定义长度为0
    // eslint-disable-next-line no-undef
    const txtval = $.trim(message)
    for (let i = 0; i < txtval.length; i++) {
      if (this.isCN(txtval.charAt(i))) {
        strlenght = strlenght + 2 // 中文为2个字符
      } else {
        strlenght = strlenght + 1 // 英文一个字符
      }
    }
    return !(strlenght > MaxLenght)
  }

  // 判断是不是中文
  isCN(str) {
    const regexCh = /[u00-uff]/
    return !regexCh.test(str)
  }

  // 验证密码6,20位
  isPassWord(str) {
    const re = /^[0-9a-zA-Z$#@^&,._+\\-]{6,20}$/
    return Boolean(re.test(str))
  }

  // 提示消息显示 成功
  isSuccess(str) {
    Notification.success({
      title: str,
      duration: 3000,
      message: ''
    })
  }

  // 提示消息显示 错误
  requestFail(str) {
    Notification.error({
      title: str,
      duration: 3000,
      message: ''
    })
  }

  // 提示消息显示 警告
  isWarning(str) {
    Notification.warning({
      title: str,
      duration: 3000,
      message: ''
    })
  }

  // json转换
  isJson(data) {
    if (typeof (data) === 'string') {
      data = JSON.parse(data)
    }
    return data
  }

  // 去重
  uniq(array) {
    return Array.from(new Set([...array]))
  }

  // 分转元
  // 日期格式转变 (转为年月日)
  changeDate(time) {
    const date = new Date()
    const year = date.getFullYear()
    let month = date.getMonth() + 1
    let strDate = date.getDate()
    if (month >= 1 && month <= 9) {
      month = '0' + month
    }
    if (strDate >= 0 && strDate <= 9) {
      strDate = '0' + strDate
    }
    return year + '年' + month + '月' + strDate
  }
  changeDate14() {
    const date = new Date()
    const y = date.getFullYear()
    let MM = date.getMonth() + 1
    MM = MM < 10 ? ('0' + MM) : MM
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    // eslint-disable-next-line no-unused-vars
    let h = date.getHours()
    h = h < 10 ? ('0' + h) : h
    // eslint-disable-next-line no-unused-vars
    let m = date.getMinutes()
    m = m < 10 ? ('0' + m) : m
    // eslint-disable-next-line no-unused-vars
    let s = date.getSeconds()
    s = s < 10 ? ('0' + s) : s
    // return y + '' + MM + '' + d + '' + h + '' + m + '' + s;
    return y + '' + MM + '' + d + '' + '08' + '' + '08' + '' + '08'
  }
  changeDate8() {
    const date = new Date()
    const y = date.getFullYear()
    let MM = date.getMonth() + 1
    MM = MM < 10 ? ('0' + MM) : MM
    let d = date.getDate()
    d = d < 10 ? ('0' + d) : d
    return y + '' + MM + '' + d
  }
  formatDate8(date) {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}${(Array(2).join('0') + date.getDate()).slice(-2)}`
  }
  formatDate6(date) {
    return `${date.getFullYear()}${('0' + (date.getMonth() + 1)).slice(-2)}`
  }
  changePT(num) {
    return (Math.round(num) / 100).toFixed(2)
  }
  // testFn(num) {
  //   num = toDecimal2(num)
  //   //将num中的$,去掉，将num变成一个纯粹的数据格式字符串
  //   num = num.toString().replace(/\$|\,/g,'');
  //   //如果num不是数字，则将num置0，并返回
  //   if(''==num || isNaN(num)){return 'Not a Number ! ';}
  //   //如果num是负数，则获取她的符号
  //   let sign = num.indexOf("-")> 0 ? '-' : '';
  //   //如果存在小数点，则获取数字的小数部分
  //   let cents = num.indexOf(".")> 0 ? num.substr(num.indexOf(".")) : '';
  //   cents = cents.length>1 ? cents : '' ;//注意：这里如果是使用change方法不断的调用，小数是输入不了的
  //   //获取数字的整数数部分
  //   num = num.indexOf(".")>0 ? num.substring(0,(num.indexOf("."))) : num ;
  //   //如果没有小数点，整数部分不能以0开头
  //   if('' == cents){ if(num.length>1 && '0' == num.substr(0,1)){return 'Not a Number ! ';}}
  //   //如果有小数点，且整数的部分的长度大于1，则整数部分不能以0开头
  //   else{if(num.length>1 && '0' == num.substr(0,1)){return 'Not a Number ! ';}}
  //   //针对整数部分进行格式化处理，这是此方法的核心，也是稍难理解的一个地方，逆向的来思考或者采用简单的事例来实现就容易多了
  //   /*
  //     也可以这样想象，现在有一串数字字符串在你面前，如果让你给他家千分位的逗号的话，你是怎么来思考和操作的?
  //     字符串长度为0/1/2/3时都不用添加
  //     字符串长度大于3的时候，从右往左数，有三位字符就加一个逗号，然后继续往前数，直到不到往前数少于三位字符为止
  //    */
  //   for (let i = 0; i < Math.floor((num.length-(1+i))/3); i++)
  //   {
  //     num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
  //   }
  //   //将数据（符号、整数部分、小数部分）整体组合返回
  //   return (sign + num + cents);
  // }
}

export default new Util()
