import Vue from 'vue'

export default function setFilter() {
  // 分转元
  Vue.filter('changeMNumber', function(num) {
    const regexp = /(?:\.0*|(\.\d+?)0+)$/
    if (num > 1000000) {
      num = JSON.stringify(num).slice(0, JSON.stringify(num).length - 4) / 100
      return num + '万'
    } else {
      num = (num / 100).toFixed(2)
      num = num.replace(regexp, '$1')
      return Number(num).toFixed(2).toString()
    }
  })
  // 元转万
  Vue.filter('changePNumber', function(num) {
    // let regexp = /(?:\.0*|(\.\d+?)0+)$/;
    if (num > 10000) {
      num = JSON.stringify(num).slice(0, JSON.stringify(num).length - 2) / 100
      return (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') + '万'
    } else {
      return (num || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')
    }
  })
  // 千位数转百分比
  Vue.filter('fPercentage', function(num) {
    return `${(Math.round(num) / 100).toFixed(2)}%`
  })
  // 八位日期 转带横杠
  Vue.filter('dateAddRod', function(str) {
    const y = str.substring(0, 4)
    const m = str.substring(4, 6)
    const d = str.substring(6, 8)
    return y + '-' + m + '-' + d
  })
}
