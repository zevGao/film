// eslint-disable-next-line no-unused-vars
import { fetchPost } from '@/utils/request'
import { fetchGet } from '@/utils/request'

export default {
  // 获取一部电影的所有合约规则
  getFilmRule(data) {
    return fetchGet(`/clear/rules/${data.filmId}/${data.subId || ''}`)
  },
  // 查询发行与院线 关系
  queryRelation(data) {
    return fetchGet(`/publish/relation/${data.filmId}/${data.publisherid}`)
  },
  // 查询院线与影院比例
  queryTheatre(data) {
    return fetchGet(`/theatre/cinema/list`, data)
  },
  getIframe() {
    return fetchGet(`webroot/decision/v5/design/report/share/cf2615bc11574efd8ae1c299d3d2e471?link=eyJhbGciOiJIUzI1NiJ9.eyJyZXBvcnRJZCI6ImNmMjYxNWJjMTE1NzRlZmQ4YWUxYzI5OWQzZDJlNDcxIiwidXNlcklkIjoiYjVmMGMyZWUtNjQwZi00MDM5LWE0ZDQtOTE4YjU1MzU0ODk4IiwianRpIjoiand0In0.mgRQLnJF7legS-55lA-7yAYgAnRmfPhACfhhbB6kmIY`)
  },
  // 影片汇总表，根据boxdate，查询所有影片'当日'票房，按票房金额倒序
  getCurrentBO(data) {
    return fetchGet(`smart/films/daily/${data.currentDate}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 影院汇总表，根据boxdate，查询所有影院'当日'票房，按票房金额倒序
  getCinemaBO(data) {
    return fetchGet(`smart/cinemas/daily/${data.currentDate}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 获取某部影片票房列表 （按日期）
  getBOFilmByDate(data) {
    return fetchGet(`smart/film/daily/${data.filmId}/${data.startDate}/${data.endDate}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 获取某影院票房列表 （按日期）
  getBOCinemaByDate(data) {
    return fetchGet(`smart/cinema/daily/${data.cinemaId}/${data.startDate}/${data.endDate}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 获取某部影片票房列表 （按影院）
  getBOFilmByCinema(data) {
    return fetchGet(`smart/film/cinema/daily/${data.currentDate}/${data.filmId}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 某影院，各部影片票房列表
  getBOFilmByFilm(data) {
    return fetchGet(`smart/cinema/film/daily/${data.currentDate}/${data.cinemaId}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 获取某部影片票房列表 （按城市）
  getBOFilmByCity(data) {
    return fetchGet(`smart/film/city/daily/${data.currentDate}/${data.filmId}/${data.offSet}/${data.row}/${data.sortName}/${data.sortType}`)
  },
  // 按影院查询某月档期内所以影片应结算给专资办、税务、院线的金额汇总
  getBillByCinema(data) {
    return fetchGet('/clear/cinema/stat', data)
  },
  // 查询院线待结算列表
  getBillByCinemaL(data) {
    return fetchGet('/clear/theatre/stat', data)
  },
  // 获取资金流水
  getCapitalFlow(data) {
    return fetchGet('/clear/capitalflow', data)
  },
  // 监票查询
  getMonitorTicket(data) {
    return fetchGet('/smart/theater/cinema/film/daily', data)
  }
}
