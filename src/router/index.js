import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // 影慧
  {
    path: '/',
    component: Layout,
    redirect: '/ticket-statistics/box-office-r-time',
    name: 'ticket-statistics',
    alwaysShow: true,
    meta: { title: '影慧', icon: 'fa fa-area-chart' },
    children: [{
      path: 'ticket-statistics/box-office-r-time',
      name: 'box-office-r-time',
      component: () => import('@/views/ticket-statistics/box-office-r-time'),
      meta: { title: '实时票房' }
    },
    {
      path: 'ticket-statistics/box-office-film',
      name: 'box-office-film',
      component: () => import('@/views/ticket-statistics/box-office-film/index'),
      // alwaysShow: true,
      meta: { title: '电影票房' },
      children: [{
        path: 'box-office-sort',
        component: () => import('@/views/ticket-statistics/box-office-film/box-office-sort/index'),
        hidden: true,
        alwaysShow: true,
        name: 'box-office-sort',
        meta: { title: '影院票房' }
      }]
    },
    {
      path: 'ticket-statistics/box-office-cinema',
      name: 'box-office-cinema',
      component: () => import('@/views/ticket-statistics/box-office-cinema/index'),
      meta: { title: '影院票房' },
      children: [{
        path: 'b-o-cinema-sort',
        component: () => import('@/views/ticket-statistics/box-office-cinema/b-o-cinema-sort/index'),
        hidden: true,
        alwaysShow: true,
        name: 'b-o-cinema-sort',
        meta: { title: '影片票房' }
      }]
    },
    {
      path: 'ticket-statistics/box-office-share',
      name: 'box-office-share',
      component: () => import('@/views/ticket-statistics/box-office-share'),
      hidden: true,
      meta: { title: '分账票房' }
    }]
  },
  // 影巡
  {
    path: '/film-patrol',
    component: Layout,
    name: 'film-patrol',
    redirect: '/film-patrol/film-query',
    meta: { title: '影巡', icon: 'fa fa-eye' },
    children: [
      {
        path: 'ticket-query',
        name: 'ticket-query',
        component: () => import('@/views/film-patrol/ticketQuery'),
        meta: { title: '监票查询' },
        children: [
          {
            path: 'ticket-details',
            name: 'ticket-details',
            component: () => import('@/views/film-patrol/ticketDetails/index'),
            hidden: true,
            meta: { title: '票号详情' }
          }
        ]
      }
    ]
  },
  // 影清
  {
    path: '/film-clear',
    component: Layout,
    name: 'film-clear',
    redirect: '/film-clear/cinema-bill',
    meta: { title: '影清', icon: 'fa fa-google-wallet' },
    alwaysShow: true,
    children: [
      {
        path: 'cinema-bill',
        name: 'clear-cinema-bill',
        component: () => import('@/views/film-clear/cinema-bill'),
        meta: { title: '影院账单' }
      },
      {
        path: 'cinemaL-bill',
        name: 'clear-cinemaL-bill',
        component: () => import('@/views/film-clear/cinemaL-bill'),
        meta: { title: '院线账单' }
      },
      {
        path: 'capitalFlow',
        name: 'capitalFlow',
        component: () => import('@/views/film-clear/capitalFlow'),
        meta: { title: '资金流水' }
      },
      {
        path: 'film-share',
        name: 'film-share',
        component: () => import('@/views/film-clear/film-share'),
        meta: { title: '电影分账查询' }
      },
      {
        path: 'film-contract',
        name: 'film-contract',
        component: () => import('@/views/film-clear/film-contract/index'),
        meta: { title: '电影合约' },
        children: [
          {
            path: 'details',
            name: 'film-contract-details',
            component: () => import('@/views/film-clear/film-contract/film-contract-details/index'),
            hidden: true,
            meta: { title: '合约详情' },
            children: [
              {
                path: 'cinema',
                name: 'cinema-contract-details',
                component: () => import('@/views/film-clear/film-contract/film-contract-details/cinema-contract-details/index'),
                hidden: true,
                meta: { title: '影院合约' }
              }
            ]
          },
          {
            path: 'contract',
            name: 'contract-info',
            component: () => import('@/views/film-clear/film-contract/contract/index'),
            hidden: true,
            meta: { title: '合约详情' },
            children: [
              {
                path: 'pubThRelation',
                name: 'pubThRelation',
                component: () => import('@/views/film-clear/film-contract/contract/tab/pubThRelation'),
                hidden: true,
                meta: { title: '院线与发行方关系' },
                children: [
                  {
                    path: 'theatreRatio',
                    name: 'theatreRatio',
                    component: () => import('@/views/film-clear/film-contract/contract/tab/theatreRatio/index'),
                    hidden: true,
                    meta: { title: '院线合约' }
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  // 影链
  {
    path: '/chain-management',
    component: Layout,
    redirect: '/chain-management/home',
    alwaysShow: true,
    name: 'chain-management',
    meta: {
      title: '影链',
      icon: 'el-icon el-icon-s-operation'
    },
    children: [
      {
        path: 'home',
        component: () => import('@/views/chain-management/home'), // Parent router-view
        name: 'chain-home',
        hidden: true,
        meta: { title: '链首页' }
      },
      {
        path: 'browser',
        component: () => import('@/views/chain-management/browser'),
        name: 'chain-browser',
        meta: { title: '浏览器' }
      }
    ]
  },
  // 帐户管理
  {
    path: '/account',
    component: Layout,
    redirect: '/account/personal-info',
    alwaysShow: true,
    name: 'account',
    meta: {
      title: '帐户管理',
      icon: 'fa fa-users'
    },
    children: [
      {
        path: 'personal-info',
        component: () => import('@/views/account/personal-info'), // Parent router-view
        name: 'personal-info',
        meta: { title: '我的信息' }
      },
      {
        path: 'account-list',
        component: () => import('@/views/account/account-list'),
        name: 'account-list',
        meta: { title: '帐户列表' }
      }
    ]
  },
  // 外部链接
  {
    path: 'external-link',
    component: Layout,
    hidden: true,
    children: [
      {
        path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
