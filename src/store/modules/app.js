import Cookies from 'js-cookie'
const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  device: 'desktop',
  // 电影票房点击电影 记录
  selectFilm: window.localStorage.getItem('selectFilm'),
  // 影院票房点击电影 记录
  selectCinema: window.localStorage.getItem('selectCinema'),
  // 监票管理点击监票数据 记录
  monitorTicketInfo: window.localStorage.getItem('monitorTicketInfo'),
  // 电影合约信息 记录
  contractInfo: window.localStorage.getItem('contractInfo'),
  // 加载动画
  showLoading: false,
  // 分页默认展示页数
  defaultRow: 10
}

const mutations = {
  UPDATE_LOADING(state, data) {
    state.showLoading = data
  },
  SET_CONTRACT_INFO: (state, data) => {
    state.contractInfo = data
    window.localStorage.setItem('contractInfo', data)
  },
  SET_SELECT_FILM: (state, data) => {
    state.selectFilm = data
    window.localStorage.setItem('selectFilm', data)
  },
  SET_SELECT_CINEMA: (state, data) => {
    state.selectCinema = data
    window.localStorage.setItem('selectCinema', data)
  },
  SET_MONITOR_INFO: (state, data) => {
    state.monitorTicketInfo = data
    window.localStorage.setItem('monitorTicketInfo', data)
  },
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  }
}

const actions = {
  setSelectFilm({ commit }, data) {
    commit('SET_SELECT_FILM', data)
  },
  setSelectCinema({ commit }, data) {
    commit('SET_SELECT_CINEMA', data)
  },
  setContractInfo({ commit }, data) {
    commit('SET_CONTRACT_INFO', data)
  },
  setMonitorTicketInfo({ commit }, data) {
    commit('SET_MONITOR_INFO', data)
  },
  updateLoading({ commit }, data) {
    commit('UPDATE_LOADING', data)
  },
  toggleSideBar({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },

  closeSideBar({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
