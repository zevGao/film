import { fetchPost } from '@/utils/request'
// eslint-disable-next-line no-unused-vars
import { fetchGet } from '@/utils/request'
// eslint-disable-next-line no-unused-vars
import { fetchPut } from '@/utils/request'

export default {
  // 影院结算至院线
  clearCinemaToL(data) {
    return fetchPost('/clear/cinema', data)
  },
  // 院线结算至制片方、发行方
  clearLToCustomer(data) {
    return fetchPost('/clear/theatre', data)
  }
}
