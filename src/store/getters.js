const getters = {
  showLoading: state => state.app.showLoading,
  selectFilm: state => state.app.selectFilm,
  selectCinema: state => state.app.selectCinema,
  monitorTicketInfo: state => state.app.monitorTicketInfo,
  contractInfo: state => state.app.contractInfo,
  defaultRow: state => state.app.defaultRow,
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name
}
export default getters
